import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxDriverLogLevel;
import org.openqa.selenium.firefox.FirefoxOptions;

public class GeckodriverBug {
    public static void main(String[] args) {
        FirefoxOptions options = new FirefoxOptions();
        options.setLogLevel(FirefoxDriverLogLevel.TRACE);
        WebDriver driver = new FirefoxDriver(options);

        driver.manage().window().setSize(new Dimension(768, 1024));
        driver.manage().window().setSize(new Dimension(768, 1024));

        driver.quit();
    }
}
